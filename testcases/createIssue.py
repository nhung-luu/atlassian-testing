# Test for creating issue
__author__ = 'nhungluu'

import sys, os, random
sys.path.append(os.path.dirname(sys.argv[0]) + "../")
from actions import utilities, issueLib, authLib
from settings import configuration

web = configuration.url
userInfo = {"username": configuration.username,
            "password": configuration.password
            }

issueInfo = {"name": configuration.projectName + " (" + configuration.projectCode + ") ",
             "type": configuration.issueType,
             "summary": configuration.issueSummary,
             "priority": configuration.issuePriority[1], # random.choice(configuration.issuePriority),
             "description": configuration.issueDescription,
             "file": configuration.filename
           }
bType = configuration.runningBrowser
testID = "Create01"

def create():
    browser = utilities.openBrowser(bType)
    utilities.setupBrowser(browser,web)
    try:
        authLib.login(browser, web, userInfo, testID)
        issueLib.createIssue(browser, issueInfo, testID)
        print "*** Result: True"
    except Exception as e:
        print "*** Result: Fail"
    finally:
        browser.close()
def main():
    print "Running test......."
    create()

main()
