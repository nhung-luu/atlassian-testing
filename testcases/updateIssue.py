# Test for updating issue
__author__ = 'nhungluu'

import sys, os, random
sys.path.append(os.path.dirname(sys.argv[0]) + "/../../")
from actions import utilities, issueLib, authLib
from settings import configuration

web = configuration.url
userInfo = {"username": configuration.username,
            "password": configuration.password
            }

issueInfo = {
             "summary": configuration.issueSummary
           }

description = configuration.issueDescription
description.append("update")
issueInfoNew = {
             "summary": configuration.issueSummary + " update",
             "priority": configuration.issuePriority[2],
             "description": description
           }

bType = configuration.runningBrowser
testID = "Update01"

def update():
    browser = utilities.openBrowser(bType)
    utilities.setupBrowser(browser,web)
    try:
        authLib.login(browser, web, userInfo, testID)
        issueLib.updateIssue(browser, issueInfo, issueInfoNew, testID)
        print "*** Result: True"
    except Exception as e:
        print "*** Result: Fail"
    finally:
        browser.close()
def main():
    print "Running test......."
    update()

main()
