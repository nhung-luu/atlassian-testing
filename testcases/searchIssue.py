# Test for searching issue
__author__ = 'nhungluu'

import sys, os, random
sys.path.append(os.path.dirname(sys.argv[0]) + "/../../")
from actions import utilities, issueLib, authLib
from settings import configuration

web = configuration.url
userInfo = {"username": configuration.username,
            "password": configuration.password
            }

issueInfo = {"name": configuration.projectName + " (" + configuration.projectCode + ") ",
             "type": configuration.issueType,
             "summary": configuration.issueSummary,
             "priority": configuration.issuePriority[1],
             "description": configuration.issueDescription
           }
bType = configuration.runningBrowser
testID = "Search01"

def search():
    browser = utilities.openBrowser(bType)
    utilities.setupBrowser(browser,web)
    try:
        authLib.login(browser, web, userInfo, testID)
        issueLib.searchIssue(browser, issueInfo, testID)
        print "*** Result: True"
    except Exception as e:
        print "*** Result: Fail"
    finally:
        browser.close()
def main():
    print "Running test......."
    search()

main()
