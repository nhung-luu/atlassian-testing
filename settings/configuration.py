__author__ = 'nhungluu'
# This Python file uses the following encoding: utf-8
import sys, os

url = "https://nhungluu.atlassian.net"

# Location to save screenshots, logs
tempDir = "C:\\tempdir\\"
tempDirMac = "/Users/nhungluu/Documents/tempdir/"

browserType = ["IE", "FF", "Chrome", "Safari"]
runningBrowser = "FF"
iePath = "C:\\tempFiles\\IEDriverServer.exe"
chromePath = "C:\\tempFiles\\chromedriver.exe"

# Login credentials
username = "nhungluu"
password = "Interview1@34="

# Input for creating new issue
projectName = "AutomationTEST"
projectCode = "AUT"
issueType = "Task"
issueSummary = "Verify user can create new issue"
issuePriority = ["Highest", "High", "Medium", "Low", "Lowest"]
issueDescription = ["Issue description:", "Second line", "Third line"]
filename = "sample_file.png"