# All authentication actions such as login, logout
# Functions:
#
__author__ = 'nhungluu'
from time import sleep
import utilities

def login(browser, web, userInfo, testID):
    returnString = "" # Contains message
    retVal = False
    try:
        print "Open JIRA site"
        browser.get(web)
        returnString = " Browser was unable to open web page.\n"
        sleep(1)
        if browser.page_source.find("Sign in")==-1:
            raise
        print "JIRA site was successfully loaded."
        returnString = " Failed to login.\n"
        browser.find_element_by_id("username").send_keys(userInfo["username"])
        browser.find_element_by_id("password").send_keys(userInfo["password"])
        browser.find_element_by_id("login").click()
        sleep(1)
        if browser.page_source.find("System Dashboard")==-1:
            raise
        returnString = "Login successfully.\n"
        retVal = True
    finally:
        if not retVal:
            utilities.result(retVal, returnString, browser, testID)