__author__ = 'nhungluu'
import sys, os, time
from time import strftime, sleep
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../")
from settings import configuration
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

# get current time and date
def getTimeStamp():
    timeStamp = strftime("%d_%b_%Y_%H_%M_%S")
    return timeStamp


# create test directory for saving log, image and others
# @param testID is type string for test case ID
def createNewDirectory(testID):
    if 'posix' in os.name:
        newTestDir = configuration.tempDirMac + testID
        if not os.path.isdir(configuration.tempDirMac):
            os.mkdir(configuration.tempDirMac)
    else:
        newTestDir = configuration.tempDir + testID
    createNewDirCommand = "mkdir " + newTestDir
    os.system(createNewDirCommand)
    return newTestDir

def setupBrowser(browser, web):
    browser.implicitly_wait(10)
    browser.maximize_window()
    browser.get(configuration.url)
    time.sleep(2)

def getBrowserType(browser):
    try:
        return str(browser).split('.')[2]
    except:
        return "unknown"

# open browser using webdriver for running test
def openBrowser(bType, firebug = True):

    browserType = configuration.browserType
    iePath = configuration.iePath
    chromePath = configuration.chromePath

    if bType in browserType:
        print "Opening browser:", bType
    else:
        print "Not support, use FireFox as default"
        bType = "FF"
    if bType == "IE":
        return webdriver.Ie(iePath)
    elif bType == "Safari":
        return webdriver.Safari()
    elif bType == "Chrome":
        return webdriver.Chrome(chromePath)
    else:
        return webdriver.Firefox()

# take screen shot when test fails
def takeScreenShot(browser, newTestDir, reason=""):
    os.chdir(newTestDir)
    fn = "{0}_{1}_{2}.jpg".format(reason, getBrowserType(browser), getTimeStamp())
    print 'Save to ' + fn
    browser.save_screenshot(fn)


# for test result and screen shot
def result(retVal, returnString, browser, testID):
    newDir = createNewDirectory(testID)
    timeStamp = getTimeStamp()
    returnString = timeStamp + ' ' + returnString
    print returnString
    if not retVal:
        takeScreenShot(browser, newDir, os.path.splitext(os.path.basename(__file__))[0])
        raise

# Input text into a field, property used is ID
# (currently all field has its unique ID, to be updated later for other properties)
def inputText(browser, field, text):
    try:
        WebDriverWait(browser, 10).until(EC.element_to_be_clickable((By.ID,field)))
        sleep(1)
        browser.find_element_by_id(field).click()
        browser.find_element_by_id(field).clear()
        sleep(0.3)
        # Selenium is having a bug that can't type open bracket ( character
        if "(" in text:
            text.replace("(", (Keys.SHIFT, "9"))
        ActionChains(browser).send_keys(text).perform()
        sleep(1)
    except Exception as e:
        print e.message

# Check if a @message is shown on the screen, try within a @count seconds
def checkMsg(browser, message, count):
    x = 0
    import re
    found = re.search(message, browser.page_source)
    while not bool(found):
        print "Wait 1s and try to get the message again."
        sleep(1)
        found = re.search(message, browser.page_source)
        print found
        x = x + 1
        if x == count:
            break
    return bool(found)
