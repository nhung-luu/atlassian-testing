#All issues related actions, create new issue, search issue, update issue
from actions import authLib

__author__ = 'nhungluu'
import os, sys, re
from time import sleep
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import utilities
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "../")
from settings import configuration

# Create a new issue
def createIssue(browser, issueInfo, testID):
    returnString = "" # Contains message
    retVal = False
    try:
        print "Start creating issue.."
        returnString = "Failed to click Create Issue."
        browser.find_element_by_id("create_link").click()
        print "Project name is ", issueInfo["name"]
        utilities.inputText(browser, "project-field", issueInfo["name"])
        utilities.inputText(browser, "priority-field", issueInfo["priority"])
        for eLine in issueInfo["description"]:
            sleep(0.1)
            browser.find_element_by_id("description").send_keys(eLine+"\n")
        returnString = "Can't type in the Summary."
        utilities.inputText(browser, "summary", issueInfo["summary"])
        # returnString ="Can't upload file."
        # browser.find_elements_by_css_selector('issue-drop-zone__file ignore-inline-attach upfile]')[0].send_keys(configuration.tempDirMac & configuration.filename)

        returnString = "Failed to create new issue."
        browser.find_element_by_id("create-issue-submit").click()
        sleep(0.3)

        returnString = "Confirmation message is not shown."
        message = configuration.projectCode + "-[0-9]{0,4}\s+-\s+" + issueInfo["summary"] + "\s*</a>\s*has been successfully created."
        sleep(1)
        msg_shown = utilities.checkMsg(browser, message, 5)
        if not msg_shown:
            raise

        returnString = "Create issue successfully.\n"
        retVal = True
    finally:
        utilities.result(retVal, returnString, browser, testID)

# Search for an issue based on its summary and verify the result
# Can be improved later with advance search
def searchIssue(browser, issueInfo, testID):
    returnString = "" # Contains message
    retVal = False
    try:
        if not simpleSearch(browser, issueInfo):
            raise
         # Verify issue detail shown is correct
        if not verifyIssueInfo(browser, issueInfo):
            print "here"
            raise
        returnString = "Search issue successfully."
        retVal = True
    finally:
        utilities.result(retVal, returnString, browser, testID)

# Quick search for an issue
def simpleSearch(browser, issueInfo):
    returnString = ""
    retVal = False
    try:
        print "Start searching issue.."
        returnString = "Failed to click Input search term."
        browser.find_element_by_id("quickSearchInput").send_keys(issueInfo["summary"] + Keys.ENTER)
        sleep(5)
        returnString = "Failed to show search result."
        #  At least 1 record must be shown
        result = browser.find_elements_by_css_selector("span.issue-link-summary")
        if len(result) == 0:
            raise
        # View first record if it is not already opened
        result[0].click()
        sleep(3)
        returnString = "Done searching issue."
        retVal = True
    finally:
        print returnString
        return retVal, returnString


# Update an existing issue
def updateIssue(browser, issueInfo, issueInfoNew, testID):
    returnString = ""
    retVal = False
    try:
        if not simpleSearch(browser, issueInfo):
            raise
        issueID = browser.find_element_by_id("key-val").text
        print "Start updating info.."
        returnString = "Failed to input data to update."
        WebDriverWait(browser, 10).until(EC.element_to_be_clickable((By.LINK_TEXT,"Edit")))
        browser.find_element_by_link_text("Edit").click()
        sleep(3)
        utilities.inputText(browser, "summary", issueInfoNew["summary"])
        utilities.inputText(browser, "priority-field", issueInfoNew["priority"])
        WebDriverWait(browser, 10).until(EC.element_to_be_clickable((By.ID,"description")))
        browser.find_element_by_id("description").clear()
        for eLine in issueInfoNew["description"]:
            sleep(0.1)
            browser.find_element_by_id("description").send_keys(eLine+"\n")
        WebDriverWait(browser, 10).until(EC.element_to_be_clickable((By.ID,"edit-issue-submit")))
        browser.find_element_by_id("edit-issue-submit").click()
        sleep(1)

        returnString = "Confirmation message is not shown"
        message = issueID + " has been updated."
        sleep(1)
        msg_shown = re.search(message, browser.page_source)
        # msg_shown = utilities.checkMsg(browser, message, 10)
        if not msg_shown:
            raise

        returnString = "Update issue successfully.\n"
        retVal = True

    finally:
        utilities.result(retVal, returnString, browser, testID)

# Verify issue summary, issue priority & description are shown correctly
def verifyIssueInfo(browser, issueInfo):
    returnString = "" # Contains message
    retVal = False
    try:
        print "Start verifying issue info"
        returnString = "Issue summary is shown incorrectly."
        sleep(3)
        title = browser.find_element_by_id("summary-val").text
        if title <> issueInfo["summary"]:
            raise

        returnString = "Priority is shown incorrectly."
        priority = browser.find_element_by_id("priority-val").text
        if priority <> issueInfo["priority"]:
            raise

        returnString = "Description is shown incorrectly"
        description = browser.find_element_by_id("description-val").text.split("\n")
        if description <> issueInfo["description"]:
            raise

        returnString = "Search issue successfully.\n"
        retVal = True
    finally:
        return retVal, returnString